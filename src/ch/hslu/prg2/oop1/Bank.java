package ch.hslu.prg2.oop1;

import java.util.ArrayList;

/**
 * Created by robinb on 23.02.2016.
 */
public class Bank {

    private String name;
    private ArrayList<Konto> accountList = new ArrayList<>();

    public Bank(String name) {
        this.name = name;
    }

    public void openKonto(Konto konto){
        accountList.add(konto);
    }

    public void openSpar(Spar spar){
        accountList.add(spar);
    }

    public int noOfAccs(){
        return accountList.size();
    }

    public void printAccounts(){
        accountList.forEach(c -> c.print());
    }

    public void printFund(){
        double sum = 0;
        for (Konto konto : accountList) {
            sum += konto.getSaldo();
        }
        System.out.println("Total Fund: " + sum);
    }
}
