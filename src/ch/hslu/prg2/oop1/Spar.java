package ch.hslu.prg2.oop1;

/**
 * Created by robinb on 23.02.2016.
 */
public class Spar extends Konto {

    private double maxOut;

    public Spar(double saldo, double rate, double maxOut) {
        super(saldo, rate);
        this.maxOut = maxOut;
    }

    public void payOut(double amount){
        if (amount <= maxOut){
            super.payOut(amount);
        }
    }

    public void print(){
        super.print();
        System.out.println("Maxout: " + maxOut);
    }
}
