package ch.hslu.prg2.oop1;

/**
 * Created by robinb on 23.02.2016.
 */
public class Main {

    public static void main(String[] args) {
        Bank bank = new Bank("Bürgi Bank");

        Konto k = new Konto(250, 0.25);
        Konto k2 = new Konto(350, 2.25);
        Konto k3 = new Konto(550, 1.25);

        Spar s = new Spar(250, 0.25, 100);
        Spar s2 = new Spar(550, 0.25, 100);
        Spar s3 = new Spar(850, 0.25, 100);

        bank.openKonto(k);
        bank.openKonto(k2);
        bank.openKonto(k3);

        bank.openSpar(s);
        bank.openSpar(s2);
        bank.openSpar(s3);

        bank.printAccounts();
        bank.printFund();
    }
}
