package ch.hslu.prg2.oop1;

/**
 * Created by robinb on 23.02.2016.
 */
public class Konto {

    public static int count = 1;
    private int no;
    private double saldo;
    private double rate;

    public Konto(){
        no = count;
        count++;
    }

    public Konto(double saldo, double rate) {
        this();
        this.saldo = saldo;
        this.rate = rate;
    }

    public void payIn(double amount){
        saldo += amount;
    }

    public void payOut(double amount){
        saldo -= amount;
    }

    public void print(){
        System.out.println("### Konto nr: " + no + " ###");
        System.out.println("Saldo: " + saldo + " Zins: " + rate);
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
