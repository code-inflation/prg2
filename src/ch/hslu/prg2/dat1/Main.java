package ch.hslu.prg2.dat1;

/**
 * Created by robinb on 23.02.2016.
 */
public class Main {

    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();
        list.insert("1");
        list.insert("2");
        list.insert("3");
        list.insert("4");
        list.insert("5");
        list.insert("6");

        System.out.println(list.length());
        System.out.println(list.hasElement("2"));
        list.printReverse();
    }
}
