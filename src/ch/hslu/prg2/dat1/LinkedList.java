package ch.hslu.prg2.dat1;

/**
 * Created by robinb on 23.02.2016.
 */
public class LinkedList<T> {

    private ListNode<T> head = null;

    public boolean hasElement(T element){
        ListNode<T> currentNode = head;
        while (currentNode!=null){
            if (currentNode.getData().equals(element)){
                return true;
            }
            currentNode = currentNode.getNext();
        }
        return false;
    }

    public void insert(ListNode<T> node){
        node.setNext(head);
        head = node;
    }

    public void insert(T element){
        ListNode<T> node = new ListNode<>(element);
        insert(node);
    }

    public int length(){
        int sum = 0;
        ListNode<T> currentNode = head;
        while (currentNode!=null){
            sum++;
            currentNode = currentNode.getNext();
        }
        return sum;
    }

    public void printReverse(){
        printReverseRek(head);
    }

    private void printReverseRek(ListNode<T> node){
        if(node.getNext() != null){
            printReverseRek(node.getNext());
        }
        System.out.println(node.getData().toString());
    }
}
