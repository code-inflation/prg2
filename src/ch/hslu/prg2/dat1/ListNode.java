package ch.hslu.prg2.dat1;

/**
 * Created by robinb on 23.02.2016.
 */
public class ListNode<T> {

    private T data;
    private ListNode<T> next;

    public ListNode(T data) {
        this.data = data;
    }

    public ListNode(ListNode<T> next, T data) {
        this.next = next;
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean hasNext(){
        if (next == null) {
            return false;
        }
        return true;
    }

    public ListNode<T> getNext() {
        return next;
    }

    public void setNext(ListNode<T> next) {
        this.next = next;
    }
}
